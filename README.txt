-- SUMMARY --

Defines the following permissions to enable a user to translate a webform's
configuration without granting them the 'translate configuration' permission
needlessly.

* translate any webform
* translate own webform

Originally posted in the Webform issue queue.
Issue [#2948012]: Translate webforms permissions, following the Webform
maintainer's suggestion, this functionality moved into its own module.

For a full description of the module, visit the project page:
  http://drupal.org/project/webform_translation_permissions
To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/webform_translation_permissions


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* @todo


-- USAGE --

* @todo


-- CONTACT --

Current maintainers:
* Rob Holmes (rob-holmes) - https://www.drupal.org/u/rob-holmes
* Stefanos Petrakis (stefanos.petrakis) -
https://www.drupal.org/u/stefanospetrakis
