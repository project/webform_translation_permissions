<?php

/**
 * @file
 * Granular permissions to translate webforms.
 */

use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function webform_translation_permissions_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.webform_translation_permissions':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Defines the following permissions to enable a user to translate a webform\'s configuration without granting them the "translate configuration" permission needlessly.') . '</p>';
      // Add a link to the Drupal.org project.
      $output .= '<p>';
      $output .= t('Visit the <a href=":project_link">Webform Translation Permissions project page</a> on Drupal.org for more information.', [
        ':project_link' => 'https://www.drupal.org/project/webform_translation_permissions',
      ]);
      $output .= '</p>';

      return $output;
  }
}

/**
 * Implements hook_config_translation_info_alter().
 */
function webform_translation_permissions_config_translation_info_alter(&$info) {
  $info['webform']['class'] = 'Drupal\webform_translation_permissions\ConfigTranslation\WebformMapper';
}
